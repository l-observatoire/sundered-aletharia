{
	// World generation config options.
	"worldgen": {
		// Enable this to disable worldgen entirely. If true, the other options have no effect.
		"disable_all": false,
		"poor_uraninite_veins_per_chunk": 8,
		"uraninite_veins_per_chunk": 6,
		"dense_uraninite_veins_per_chunk": 3,
		"dry_ice_veins_per_chunk": 9
	},
	// Other general config options.
	"general": {
		// Enable this to get Player Aerial Pearl by right clicking a Zombie or Husk with a Aerial Pearl.
		"player_aerial_pearl": true,
		// Enable this to get Dimensional Binding card by right clicking an Enderman or Endermite with a Binding card.
		"dimensional_binding_card": true,
		// Enable this to get Lens Of Ender by right clicking an Enderman or Endermite with a Photoelectric Pane.
		"lens_of_ender": true,
		// List of fluids used in the Magmator.
		"magmatic_fluids": {
			"minecraft:lava": 10000
		},
		// List of coolant fluids used in the Reactor and the Thermo Generator.
		"coolant_fluids": {
			"minecraft:water": 1
		},
		// List of heat source blocks used under Thermo Generator.
		"heat_blocks": {
			"minecraft:lava": 1000,
			"powah:blazing_crystal_block": 2800,
			"minecraft:magma_block": 800
		},
		// Energy produced per fuel tick in the Furnator.
		"energy_per_fuel_tick": 8,
		"energizing_range": 4,
		/* Multiplier to the required energy applied after an energizing recipe is read.
		   Use this to adjust the cost of ALL energizing recipes.
		*/
		"energizing_energy_ratio": 0.25
	},
	// Configuration of energy values for generators.
	"generators": {
		"furnators": {
			"capacities": {
				"starter": 5000,
				"basic": 20000,
				"hardened": 50000,
				"blazing": 200000,
				"niotic": 500000,
				"spirited": 2000000,
				"nitro": 10000000
			},
			"transfer_rates": {
				"starter": 60,
				"basic": 120,
				"hardened": 400,
				"blazing": 1000,
				"niotic": 2000,
				"spirited": 8000,
				"nitro": 40000
			},
			"generation_rates": {
				"starter": 20,
				"basic": 40,
				"hardened": 100,
				"blazing": 250,
				"niotic": 500,
				"spirited": 2000,
				"nitro": 5000
			}
		},
		"magmators": {
			"capacities": {
				"starter": 5000,
				"basic": 20000,
				"hardened": 50000,
				"blazing": 200000,
				"niotic": 500000,
				"spirited": 2000000,
				"nitro": 10000000
			},
			"transfer_rates": {
				"starter": 60,
				"basic": 120,
				"hardened": 400,
				"blazing": 1000,
				"niotic": 2000,
				"spirited": 8000,
				"nitro": 40000
			},
			"generation_rates": {
				"starter": 20,
				"basic": 40,
				"hardened": 100,
				"blazing": 250,
				"niotic": 500,
				"spirited": 2000,
				"nitro": 5000
			}
		},
		"reactors": {
			"capacities": {
				"starter": 37500,
				"basic": 125000,
				"hardened": 625000,
				"blazing": 10000000,
				"niotic": 25000000,
				"spirited": 100000000,
				"nitro": 500000000
			},
			"transfer_rates": {
				"starter": 250,
				"basic": 1000,
				"hardened": 2500,
				"blazing": 10000,
				"niotic": 25000,
				"spirited": 100000,
				"nitro": 500000
			},
			"generation_rates": {
				"starter": 44,
				"basic": 75,
				"hardened": 500,
				"blazing": 1250,
				"niotic": 3125,
				"spirited": 12500,
				"nitro": 50000
			}
		},
		"solar_panels": {
			"capacities": {
				"starter": 5000,
				"basic": 20000,
				"hardened": 50000,
				"blazing": 200000,
				"niotic": 500000,
				"spirited": 2000000,
				"nitro": 10000000
			},
			"transfer_rates": {
				"starter": 40,
				"basic": 120,
				"hardened": 400,
				"blazing": 1500,
				"niotic": 5000,
				"spirited": 16000,
				"nitro": 50000
			},
			"generation_rates": {
				"starter": 10,
				"basic": 30,
				"hardened": 100,
				"blazing": 375,
				"niotic": 1250,
				"spirited": 4000,
				"nitro": 12500
			}
		},
		"thermo_generators": {
			"capacities": {
				"starter": 5000,
				"basic": 20000,
				"hardened": 50000,
				"blazing": 200000,
				"niotic": 500000,
				"spirited": 2000000,
				"nitro": 10000000
			},
			"transfer_rates": {
				"starter": 40,
				"basic": 80,
				"hardened": 200,
				"blazing": 800,
				"niotic": 2000,
				"spirited": 8000,
				"nitro": 40000
			},
			"generation_rates": {
				"starter": 10,
				"basic": 20,
				"hardened": 35,
				"blazing": 75,
				"niotic": 150,
				"spirited": 375,
				"nitro": 875
			}
		}
	},
	// Configuration of energy values for other devices.
	"devices": {
		"batteries": {
			"capacities": {
				"starter": 250000,
				"basic": 1000000,
				"hardened": 2500000,
				"blazing": 10000000,
				"niotic": 25000000,
				"spirited": 100000000,
				"nitro": 500000000
			},
			"transfer_rates": {
				"starter": 250,
				"basic": 1000,
				"hardened": 2500,
				"blazing": 10000,
				"niotic": 25000,
				"spirited": 100000,
				"nitro": 500000
			}
		},
		"cables": {
			"transfer_rates": {
				"starter": 125,
				"basic": 500,
				"hardened": 1250,
				"blazing": 5000,
				"niotic": 12500,
				"spirited": 50000,
				"nitro": 250000
			}
		},
		"dischargers": {
			"capacities": {
				"starter": 250000,
				"basic": 1000000,
				"hardened": 2500000,
				"blazing": 10000000,
				"niotic": 25000000,
				"spirited": 100000000,
				"nitro": 500000000
			},
			"transfer_rates": {
				"starter": 250,
				"basic": 1000,
				"hardened": 2500,
				"blazing": 10000,
				"niotic": 25000,
				"spirited": 100000,
				"nitro": 500000
			}
		},
		"ender_cells": {
			"transfer_rates": {
				"starter": 250,
				"basic": 1000,
				"hardened": 2500,
				"blazing": 10000,
				"niotic": 25000,
				"spirited": 100000,
				"nitro": 500000
			},
			"channels": {
				"starter": 1,
				"basic": 2,
				"hardened": 3,
				"blazing": 5,
				"niotic": 7,
				"spirited": 9,
				"nitro": 12
			}
		},
		"ender_gates": {
			"transfer_rates": {
				"starter": 125,
				"basic": 500,
				"hardened": 1250,
				"blazing": 5000,
				"niotic": 12500,
				"spirited": 50000,
				"nitro": 250000
			},
			"channels": {
				"starter": 1,
				"basic": 2,
				"hardened": 3,
				"blazing": 5,
				"niotic": 7,
				"spirited": 9,
				"nitro": 12
			}
		},
		"energy_cells": {
			"capacities": {
				"starter": 250000,
				"basic": 1000000,
				"hardened": 2500000,
				"blazing": 10000000,
				"niotic": 25000000,
				"spirited": 100000000,
				"nitro": 500000000
			},
			"transfer_rates": {
				"starter": 250,
				"basic": 1000,
				"hardened": 2500,
				"blazing": 10000,
				"niotic": 25000,
				"spirited": 100000,
				"nitro": 500000
			}
		},
		"energizing_rods": {
			"capacities": {
				"starter": 2500,
				"basic": 10000,
				"hardened": 25000,
				"blazing": 100000,
				"niotic": 250000,
				"spirited": 1000000,
				"nitro": 500000
			},
			"transfer_rates": {
				"starter": 25,
				"basic": 100,
				"hardened": 250,
				"blazing": 1000,
				"niotic": 2500,
				"spirited": 10000,
				"nitro": 50000
			}
		},
		"hoppers": {
			"capacities": {
				"starter": 250000,
				"basic": 1000000,
				"hardened": 2500000,
				"blazing": 10000000,
				"niotic": 25000000,
				"spirited": 100000000,
				"nitro": 500000000
			},
			"transfer_rates": {
				"starter": 250,
				"basic": 1000,
				"hardened": 2500,
				"blazing": 10000,
				"niotic": 25000,
				"spirited": 100000,
				"nitro": 500000
			},
			"charging_rates": {
				"starter": 125,
				"basic": 500,
				"hardened": 1250,
				"blazing": 5000,
				"niotic": 12500,
				"spirited": 50000,
				"nitro": 250000
			}
		},
		"player_transmitters": {
			"capacities": {
				"starter": 250000,
				"basic": 1000000,
				"hardened": 2500000,
				"blazing": 10000000,
				"niotic": 25000000,
				"spirited": 100000000,
				"nitro": 500000000
			},
			"transfer_rates": {
				"starter": 250,
				"basic": 1000,
				"hardened": 2500,
				"blazing": 10000,
				"niotic": 25000,
				"spirited": 100000,
				"nitro": 500000
			},
			"charging_rates": {
				"starter": 125,
				"basic": 500,
				"hardened": 1250,
				"blazing": 5000,
				"niotic": 12500,
				"spirited": 50000,
				"nitro": 250000
			}
		}
	}
}