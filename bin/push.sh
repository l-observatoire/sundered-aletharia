#!/bin/bash

set -euo pipefail

MODPACK_ROOT="/mnt/c/Users/Astra/AppData/Roaming/com.modrinth.theseus/profiles/Sundered Aletharia (1)"
#MODPACK_ROOT="/Users/astra/Library/Application Support/PrismLauncher/instances/Sundered Aletharia 2.2 2.1.0-rc2/.minecraft"

rsync -av ./config/ "$MODPACK_ROOT/config/"
rsync -av ./kubejs/ "$MODPACK_ROOT/kubejs/"
rsync -av ./patchouli_books/ "$MODPACK_ROOT/patchouli_books/"
