#!/bin/bash

set -euo pipefail

MODPACK_ROOT="/mnt/c/Users/Astra/AppData/Roaming/com.modrinth.theseus/profiles/Sundered Aletharia (1)"
#MODPACK_ROOT="/Users/astra/Library/Application Support/PrismLauncher/instances/Sundered Aletharia 2.2 2.1.0-rc2/.minecraft"

# Files we want to make sure don't exist when we publish a modpack

test -e "$MODPACK_ROOT/config/authme.json5" && rm --preserve-root -v "$MODPACK_ROOT/config/authme.json5"
test -e "$MODPACK_ROOT/config/FabricProxy-Lite.*" && rm --preserve-root -v "$MODPACK_ROOT/config/FabricProxy-Lite.*"
test -e "$MODPACK_ROOT/config/xaero*" && rm --preserve-root -v "$MODPACK_ROOT/config/xaero*"
test -e "$MODPACK_ROOT/config/spark/" && rm --preserve-root -v -r "$MODPACK_ROOT/config/spark/"
test -e "$MODPACK_ROOT/config/voicechat/" && rm --preserve-root -v -r "$MODPACK_ROOT/config/voicechat/"


# TODO:
# remove inventoryPN and inventorySorter files before publish
