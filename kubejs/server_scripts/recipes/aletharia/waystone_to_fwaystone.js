ServerEvents.recipes(event => {
  const shapelessRecipes = [
  	{
      output: 'waystones:warp_plate',
      input: ['#waystones:waystones', 'minecraft:stone_pressure_plate'],
      id: 'aletharia:waystone_to_warp_plate'
    },
    {
      output: 'fwaystones:waystone',
      input: ['#waystones:waystones', 'minecraft:stone'],
      id: 'aletharia:waystone_to_fwaystone'
    },
    {
      output: 'fwaystones:stone_brick_waystone',
      input: ['#waystones:waystones', '#minecraft:stone_bricks'],
      id: 'aletharia:waystone_to_stone_brick_fwaystone'
    },
    {
      output: 'fwaystones:deepslate_brick_waystone',
      input: ['#waystones:waystones', '#c:deepslate_bricks'],
      id: 'aletharia:waystone_to_deepslate_brick_fwaystone'
    },
    {
      output: 'fwaystones:blackstone_brick_waystone',
      input: ['#waystones:waystones', '#c:refined_blackstones'],
      id: 'aletharia:waystone_to_blackstone_brick_fwaystone'
    },
    {
      output: 'fwaystones:desert_waystone',
      input: ['#waystones:waystones', '#c:uncolored_sandstone_blocks'],
      id: 'aletharia:waystone_to_desert_fwaystone'
    },
    {
      output: 'fwaystones:red_desert_waystone',
      input: ['#waystones:waystones', '#c:red_sandstone_blocks'],
      id: 'aletharia:waystone_to_desert_fwaystone'
    },
    {
      output: 'fwaystones:nether_brick_waystone',
      input: ['#waystones:waystones', '#c:nether_bricks'],
      id: 'aletharia:waystone_to_nether_brick_fwaystone'
    },
    {
      output: 'fwaystones:red_nether_brick_waystone',
      input: ['#waystones:waystones', 'minecraft:red_nether_bricks'],
      id: 'aletharia:waystone_to_red_nether_brick_fwaystone'
    },
    {
      output: 'fwaystones:end_stone_brick_waystone',
      input: ['#waystones:waystones', 'minecraft:end_stone_bricks'],
      id: 'aletharia:waystone_to_end_stone_brick_fwaystone'
    },
  ];

  shapelessRecipes.forEach((recipe) => {
    event.shapeless(recipe.output, recipe.input);
  });
});
