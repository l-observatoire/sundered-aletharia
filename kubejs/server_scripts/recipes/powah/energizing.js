// Inspired by Team AOF, thank you.

ServerEvents.recipes(event => {

  // aletharia:powah/niotic_crystal_block
  event.custom({
    id: "aletharia:powah/niotic_crystal_block",
    type: "powah:energizing",
    ingredients: [
      { item: "minecraft:diamond_block" },
    ],
    energy: "2700000",
    result: {
      item: "powah:niotic_crystal_block",
    }
  });

  // aletharia:powah/blazing_crystal_block
  event.custom({
    id: "aletharia:powah/blazing_crystal_block",
    type: "powah:energizing",
    ingredients: [
      { item: "botania:blaze_block" },
    ],
    energy: "810000",
    result: {
      item: "powah:blazing_crystal_block",
    }
  });

  // aletharia:powah/spirited_crystal_block
  event.custom({
    id: "aletharia:powah/spirited_crystal_block",
    type: "powah:energizing",
    ingredients: [
      { item: "minecraft:emerald_block" },
    ],
    energy: "9000000",
    result: {
      item: "powah:spirited_crystal_block",
    }
  });

    // aletharia:powah/energized_steel_block
    event.custom({
    id: "aletharia:powah/energized_steel_block",
    type: "powah:energizing",
    ingredients: [
      { item: "minecraft:gold_block" },
      { item: "minecraft:iron_block" },
    ],
    energy: "90000",
    result: {
      item: "powah:energized_steel_block",
      count: 2,
    }
  });
});
