// TechReborn ores
const techreborn_ores = [
    {name: 'bauxite', normal: true, deepslate: true, rockslate: true},
    {name: 'cinnabar', normal: true, deepslate: false, rockslate: false},
    {name: 'galena', normal: true, deepslate: true, rockslate: true},
    {name: 'iridium', normal: true, deepslate: true, rockslate: true},
    {name: 'lead', normal: true, deepslate: true, rockslate: true},
    {name: 'peridot', normal: true, deepslate: true, rockslate: false},
    {name: 'pyrite', normal: true, deepslate: false, rockslate: false},
    {name: 'ruby', normal: true, deepslate: true, rockslate: true},
    {name: 'sapphire', normal: true, deepslate: true, rockslate: true},
    {name: 'sheldonite', normal: true, deepslate: true, rockslate: false},
    {name: 'silver', normal: true, deepslate: true, rockslate: true},
    {name: 'sodalite', normal: true, deepslate: true, rockslate: false},
    {name: 'sphalerite', normal: true, deepslate: false, rockslate: false},
    {name: 'tin', normal: true, deepslate: true, rockslate: true},
    {name: 'tungsten', normal: false, deepslate: true, rockslate: false},
];

ServerEvents.tags('block', event => {
    techreborn_ores.forEach((item) => {
        if (item.normal) {
    	event.add("c:ores", "techreborn:" + item.name + "_ore")
        }
        if (item.deepslate) {
            event.add("c:ores", "techreborn:deepslate_" + item.name + "_ore")
        }
        if (item.rockslate) {
            event.add("c:ores", "tbbc:rockslate_" + item.name + "_ore")
            event.add("techreborn:ores", "tbbc:rockslate_" + item.name + "_ore")
        }
    });
    event.add("c:ores", "create_new_age:thorium_ore");
});

ServerEvents.tags('item', event => {
    techreborn_ores.forEach((item) => {
        if (item.normal) {
    	    event.add("c:ores", "techreborn:" + item.name + "_ore")
        }
        if (item.deepslate) {
            event.add("c:ores", "techreborn:deepslate_" + item.name + "_ore")
        }
        if (item.rockslate) {
            event.add("c:ores", "tbbc:rockslate_" + item.name + "_ore")
            event.add("techreborn:ores", "tbbc:rockslate_" + item.name + "_ore")
        }
    });
    event.add("c:ores", "create_new_age:thorium_ore");
});