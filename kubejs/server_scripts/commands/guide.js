ServerEvents.commandRegistry(event => {
  const { commands: Commands, arguments: Arguments } = event
  
  event.register(
    Commands.literal('guide').executes(c => giveGuideBook(c.source.player))
  );
  event.register(
    Commands.literal('guidebook').executes(c => giveGuideBook(c.source.player))
  )
  
  // Helper function
  let giveGuideBook = (player) => {
    console.log(`Giving server guidebook to '${player}'`)
    player.give(Item.of('patchouli:guide_book', 1,'{"patchouli:book": "patchouli:aletharia_companion"}'))
    return 1
  }
});