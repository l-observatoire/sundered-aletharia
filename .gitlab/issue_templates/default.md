> Please include these details with your issue:
>
> - What version of the modpack are you using?
> - Are you using Windows, Mac, or Linux?
> - What were you doing when the issue occurred?
> - What did you expect to happen?
> - What happened instead?
