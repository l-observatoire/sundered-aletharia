### l'observatoire: Sundered Aletharia

[Issue Tracker](https://gitlab.com/l-observatoire/sundered-aletharia/-/issues) for the [Sundered Aletharia](https://modrinth.com/modpack/lobservatoire-sundered-aletharia) modpack, primarily to be used on my server.

#### Some flavor text, because why not?

Sundered Aletharia, an interesting place in this new world. A huge chunk of land and mountain just appeared one day. It was obvious this land was from somewhere else. The land abruptly changing and in some places it towers above the field that used to be there. A small group of people living and camping in the region now found themselves in a foreign land. Where were they going to get their medicine, what would they do with their skill sets now that they found themselves in a new world that didn't work the same way as their old one. The tasks of the day are exploration and building a new life. At least one of the denizens of this new world has setup in The Cove and ze has embraced that pirate spirit.

This modpack centers around Create and its many companion mods and coupled with tech related mods. AE2, TechReborn and Powah for some more crunchy and process oriented people. Botania for a little bit of magic. ComputerCraft because I a programmer and it intriques me. As well as some mods to help build a beautiful world. Farmer's Delight because I like food.
